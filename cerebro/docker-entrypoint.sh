#!/bin/sh

cat /opt/cerebro/conf/application.conf | \
sed "s/CEREBRO_USERNAME/${CEREBRO_USERNAME}/g" | \
sed "s/CEREBRO_PASSWORD/${CEREBRO_PASSWORD}/g" > /tmp/application.yml

mv /tmp/application.yml /opt/cerebro/conf/application.conf

exec "$@"